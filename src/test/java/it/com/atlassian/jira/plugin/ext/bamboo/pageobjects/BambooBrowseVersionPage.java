package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraTabPage;
import com.atlassian.jira.projects.pageobjects.webdriver.page.legacy.browseversion.BrowseVersionTab;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class BambooBrowseVersionPage extends AbstractJiraTabPage<BrowseVersionTab> {
    private static final String URI = "/browse/%s/fixforversion/%s";

    private final String projectKey;
    private final String versionId;

    @ElementBy(id = "project-tab")
    private PageElement projectTab;

    public BambooBrowseVersionPage(String projectKey, String versionId) {
        this.projectKey = projectKey;
        this.versionId = versionId;
    }


    @Override
    public TimedCondition isAt() {
        return projectTab.timed().isPresent();
    }

    public String getUrl() {
        return String.format(URI, projectKey, versionId);
    }
}

