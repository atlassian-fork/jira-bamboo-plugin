package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.SLOW_PAGE_LOAD;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.UI_ACTION;
import static org.openqa.selenium.By.cssSelector;

/**
 * Page element for interacting with inline dialogs, for example if we need to dismiss one so that we
 * can see an element underneath.
 */
public class InlineDialog {
    private static final String INLINE_DIALOG_SELECTOR = ".aui-inline-dialog";

    private final String dialogTypeSelector;
    private final String specificDialogSelector;

    @Inject
    private PageElementFinder pageElementFinder;

    public InlineDialog(@Nonnull final String dialogTypeSelector) {
        this.dialogTypeSelector = dialogTypeSelector;
        this.specificDialogSelector = String.format("%s %s", INLINE_DIALOG_SELECTOR, this.dialogTypeSelector);
    }

    public void dismissDialogIfVisible() {
        if (specificDialogIsVisibleByTimeout()) {
            getSpecificDialogContainerAndDismiss();
        }
    }

    private void getSpecificDialogContainerAndDismiss() {
        for (PageElement dialog : getInlineDialogsOnPage()) {
            if (isSpecificType(dialog)) {
                dismissDialog(dialog);
                break;
            }
        }
    }

    private boolean specificDialogIsVisibleByTimeout() {
        PageElement specificDialog = pageElementFinder.find(cssSelector(specificDialogSelector));
        return specificDialog.withTimeout(SLOW_PAGE_LOAD).timed().isVisible().byDefaultTimeout();
    }

    private void dismissDialog(@Nonnull final PageElement dialog) {
        dialog.find(cssSelector("button")).click();
        waitUntilFalse(dialog.withTimeout(UI_ACTION).timed().isVisible());
    }

    private boolean isSpecificType(@Nonnull final PageElement dialog) {
        return dialog.find(cssSelector(dialogTypeSelector)).isPresent();
    }

    @Nonnull
    private List<PageElement> getInlineDialogsOnPage() {
        return pageElementFinder.findAll(cssSelector(INLINE_DIALOG_SELECTOR));
    }
}
