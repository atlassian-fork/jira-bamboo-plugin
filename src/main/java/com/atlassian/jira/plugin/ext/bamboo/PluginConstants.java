package com.atlassian.jira.plugin.ext.bamboo;

public final class PluginConstants {
    /**
     * Keys for storing release information in Plugin Settings (PS)
     */
    public static final String PS_CONFIG_DATA_KEY = "bamboo.release.config.";
    public static final String PS_CONFIG_DEFAUTS_KEY = "bamboo.release.config.defaults";
    public static final String PS_CONFIG_PLAN = "plan";
    public static final String PS_CONFIG_STAGE = "stage";
    public static final String PS_CONFIG_USER_NAME = "user";
    public static final String PS_CONFIG_OPEN_ISSUES = "openIssuesAction";
    public static final String PS_CONFIG_OPEN_ISSUES_VERSION = "openIssuesMoveVersion";
    public static final String PS_CONFIG_BUILD_TYPE = "buildType";
    public static final String PS_CONFIG_RELEASE_DATE = "releaseDate";

    public static final String ISSUE_ACTION_IGNORE = "ignore";
    public static final String ISSUE_ACTION_MOVE = "move";

    public static final String PS_BUILD_DATA_KEY = "bamboo.release.build.";
    public static final String PS_BUILD_COMPLETED_STATE = "state";
    public static final String PS_BUILD_RESULT = "result";
    public static final String PS_RELEASE_COMPLETE = "completed";

    public static final String PS_RELEASE_ERRORS = "bamboo.release.errors.";

    /**
     * variables for UI are prefixed with this
     **/
    public static final String VARIABLE_PARAM_PREFIX = "variable_";
    /**
     * variables for REST requests are prefixed with this
     **/
    public static final String VARIABLE_REST_PREFIX = "bamboo.variable.";

    public static final String CREDENTIALS_REQUIRED = "Credentials required";

    public static final String LOGIN_AND_APPROVE_BEFORE_CONTINUING_I18N_KEY = "bamboo.loginAndApprove.release";
    public static final String LOGIN_AND_APPROVE_BEFORE_STATUS_VISIBLE_I18N_KEY = "bamboo.loginAndApprove.release.status";
    public static final String LOGIN_AND_APPROVE_BEFORE_DEPLOYMENT_VISIBLE_I18N_KEY = "bamboo.loginAndApprove.deployments";
    public static final String BAMBOO_UNREACHABLE_TITLE_I18N_KEY = "bamboo.unreachable";

    public static final String BAMBOO_ERROR_CONNECTIVITY_I18N_KEY = "bamboo.error.connectivity";
    public static final String BAMBOO_ERROR_CONNECTIVITY_CONTINUE_RELEASE_I18N_KEY = "bamboo.error.connectivity.continue.jira.release";
    public static final String BAMBOO_ERROR_RELEASE_RETRY_MANUALLY_I18N_KEY = "bamboo.error.release.retry.manually";
    public static final String JIRA_ERROR_NOT_RECOVERABLE_I18N_KEY = "jira.error.not.recoverable";

    public static final String ERROR_INVALID_VERSION_I18N_KEY = "bamboo.error.invalid.version";
    public static final String ERROR_USER_NOT_AUTHORISED_I18N_KEY = "bambo.error.user.not.authorised";
    public static final String ERROR_INVALID_PLAN_I18N_KEY = "bamboo.error.invalid.plan";
    public static final String ERROR_VERSION_WITHOUT_PROJECT_I18N_KEY = "bamboo.error.version.without.project";
    public static final String ERROR_CONFIGURATION_I18N_KEY = "bamboo.error.applinks.configuration";
    public static final String ADMIN_ERROR_VERSION_NO_PERMISSION_I18N_KEY = "admin.errors.version.no.permission";

    public static final String BUILD_TYPE_NO_BUILD = "no-build";
    public static final String BUILD_TYPE_NEW_BUILD = "new-build";
    public static final String BUILD_TYPE_EXISTING_BUILD = "existing-build";

    public static final String DEFAULT_SERVER_NAME = "Bamboo";

    private PluginConstants() {
    }
}
