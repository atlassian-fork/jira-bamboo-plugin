package com.atlassian.jira.plugin.ext.bamboo.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents the Status of a Plan on a Bamboo server
 */
public class PlanStatus {
    private final PlanKey planKey;
    private final BuildState latestBuildState;
    private final LifeCycleState latestLifeCycleState;

    public static PlanStatus fromJsonObject(PlanKey planKey, JSONObject jsonObject) throws JSONException {
        final String state = jsonObject.getString("state");
        final String lifecycle = jsonObject.getString("lifeCycleState");

        final BuildState buildState = BuildState.getInstance(state);
        final LifeCycleState lifeCycleState = LifeCycleState.getInstance(lifecycle);

        return new PlanStatus(planKey, buildState, lifeCycleState);
    }

    public PlanStatus(final PlanKey planKey, final BuildState buildState, final LifeCycleState lifeCycleState) {
        this.planKey = planKey;
        this.latestBuildState = buildState;
        this.latestLifeCycleState = lifeCycleState;
    }

    /**
     * @return planKey
     */
    @Nonnull
    public PlanKey getPlanKey() {
        return planKey;
    }

    /**
     * @return the buildState of the latest result for that plan (note: that might be latest *finished* result - I'm not sure)
     * If plan has ever been built it will return null
     */
    @Nullable
    public BuildState getLatestBuildState() {
        return latestBuildState;
    }

    /**
     * @return the lifeCycleState of the latest result for that plan (note: that might be latest *finished* result - I'm not sure)
     * If plan has ever been built it will return null
     */
    @Nullable
    public LifeCycleState getLatestLifeCycleState() {
        return latestLifeCycleState;
    }

    @Override
    public String toString() {
        return "PlanStatus{" +
                "planKey=" + planKey +
                ", latestBuildState=" + latestBuildState +
                ", latestLifeCycleState=" + latestLifeCycleState +
                '}';
    }
}
