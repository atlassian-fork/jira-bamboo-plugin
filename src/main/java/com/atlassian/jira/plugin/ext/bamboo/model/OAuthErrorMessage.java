package com.atlassian.jira.plugin.ext.bamboo.model;

import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.Map;

public class OAuthErrorMessage extends ErrorMessage {
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties

    private final URI oauthCallbackUri;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors

    public OAuthErrorMessage(String description, URI oauthCallbackUri) {
        super("Authentication Required", description);
        this.oauthCallbackUri = oauthCallbackUri;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods

    @Nonnull
    @Override
    protected Map<String, String> getExtraValues() {
        final Map<String, String> map = Maps.newHashMap();
        map.put("oauthCallback", oauthCallbackUri.toString());
        return map;
    }

    // ------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
