package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.UpdateIssueRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.ext.bamboo.PluginConstants;
import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.release.ReleaseFinalisingAction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.ManagedLock;
import com.atlassian.util.concurrent.ManagedLocks;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import static com.atlassian.jira.component.ComponentAccessor.getApplicationProperties;
import static com.atlassian.jira.event.type.EventDispatchOption.ISSUE_UPDATED;
import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_RELEASE_RETRY_MANUALLY_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.JIRA_ERROR_NOT_RECOVERABLE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_COMPLETED_STATE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_DATA_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_RESULT;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_OPEN_ISSUES;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_RELEASE_DATE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_USER_NAME;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.VARIABLE_PARAM_PREFIX;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.VARIABLE_REST_PREFIX;
import static com.atlassian.jira.plugin.ext.bamboo.model.BuildState.SUCCESS;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

@ExportAsService({BambooReleaseService.class, LifecycleAware.class})
@Component
public class BambooReleaseServiceImpl implements BambooReleaseService, LifecycleAware {
    private static final Logger log = Logger.getLogger(BambooReleaseServiceImpl.class);

    private static final String BAMBOO_TRIGGER_REASON_JIRA_BASE_URL = "bamboo.triggerReason.jiraBaseUrl";
    private static final String BAMBOO_TRIGGER_REASON_JIRA_PROJECT_KEY = "bamboo.triggerReason.jiraProjectKey";
    private static final String BAMBOO_TRIGGER_REASON_JIRA_PROJECT_NAME = "bamboo.triggerReason.jiraProjectName";
    private static final String BAMBOO_TRIGGER_REASON_JIRA_USER_DISPLAY_NAME = "bamboo.triggerReason.jiraUserDisplayName";
    private static final String BAMBOO_TRIGGER_REASON_JIRA_USERNAME = "bamboo.triggerReason.jiraUsername";
    private static final String BAMBOO_TRIGGER_REASON_JIRA_VERSION_ID = "bamboo.triggerReason.jiraVersionId";
    private static final String BAMBOO_TRIGGER_REASON_JIRA_VERSION_NAME = "bamboo.triggerReason.jiraVersion";
    private static final String CUSTOM_PLAN_TRIGGER_KEY = "bamboo.customPlanTriggerKey";
    private static final String CUSTOM_TRIGGER_REASON_KEY = "bamboo.customTriggerReasonKey";
    private static final String PLUGIN_PREFIX = "com.atlassian.bamboo.plugin.jira";
    private static final String PLAN_TRIGGER = PLUGIN_PREFIX + ":jiraReleasePlanTrigger";
    private static final String TRIGGER_REASON = PLUGIN_PREFIX + ":jiraReleaseTriggerReason";

    private final Function<Version, ManagedLock> versionReleaseLocks = ManagedLocks.weakManagedLockFactory();

    private final BambooRestService bambooRestService;
    private final DateFieldFormat dateFieldFormat;
    private final GlobalPermissionManager globalPermissionManager;
    private final IssueManager issueManager;
    private final JiraAuthenticationContext authenticationContext;
    private final JiraPropertySetFactory jiraPropertySetFactory;
    private final PermissionManager permissionManager;
    private final PlanResultStatusUpdateService planStatusUpdateService;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ProjectManager projectManager;
    private final ReleaseErrorReportingService releaseErrorReportingService;
    private final SearchService searchService;
    private final UserManager userManager;
    private final VersionManager versionManager;

    @Autowired
    public BambooReleaseServiceImpl(
            @ComponentImport final DateFieldFormat dateFieldFormat,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final IssueManager issueManager,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final JiraPropertySetFactory jiraPropertySetFactory,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final PluginSettingsFactory pluginSettingsFactory,
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final SearchService searchService,
            @ComponentImport("jiraUserManager") final UserManager userManager,
            @ComponentImport final VersionManager versionManager,
            final BambooRestService bambooRestService,
            final PlanResultStatusUpdateService planStatusUpdateService,
            final ReleaseErrorReportingService releaseErrorReportingService) {
        this.authenticationContext = checkNotNull(authenticationContext);
        this.bambooRestService = checkNotNull(bambooRestService);
        this.dateFieldFormat = checkNotNull(dateFieldFormat);
        this.globalPermissionManager = checkNotNull(globalPermissionManager);
        this.issueManager = checkNotNull(issueManager);
        this.jiraPropertySetFactory = checkNotNull(jiraPropertySetFactory);
        this.permissionManager = checkNotNull(permissionManager);
        this.planStatusUpdateService = checkNotNull(planStatusUpdateService);
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory);
        this.projectManager = checkNotNull(projectManager);
        this.releaseErrorReportingService = checkNotNull(releaseErrorReportingService);
        this.searchService = checkNotNull(searchService);
        this.userManager = checkNotNull(userManager);
        this.versionManager = checkNotNull(versionManager);
    }

    @Nonnull
    public PlanExecutionResult triggerPlanForRelease(
            @Nonnull final ApplicationLink applicationLink,
            @Nonnull final Version version,
            @Nonnull final Map<String, String> settings)
            throws CredentialsRequiredException {
        final Project project = version.getProject();
        final String projectKey = project.getKey();

        clearErrors(version);
        clearBuildData(projectKey, version.getId());
        saveConfigData(projectKey, version, settings);

        final Map<String, String> params = populateParams(version, settings, project);

        PlanKey planKey = PlanKeys.getPlanKey(settings.get(PluginConstants.PS_CONFIG_PLAN));
        String stage = settings.get(PluginConstants.PS_CONFIG_STAGE);
        final RestResult<PlanResultKey> restResult = bambooRestService.triggerPlan(applicationLink, planKey, stage, params);

        PlanResultKey planResultKey = restResult.getResult();
        if (planResultKey != null && restResult.getErrors().isEmpty()) {
            String userName = settings.get(PS_CONFIG_USER_NAME);
            subscribeToReleaseStatus(version, userName, planResultKey);
            registerReleaseStarted(projectKey, version.getId(), planResultKey);
        }

        return new PlanExecutionResult(restResult.getResult(), restResult.getErrors());
    }

    @Nonnull
    public PlanExecutionResult triggerExistingBuildForRelease(@Nonnull final ApplicationLink applicationLink,
                                                              @Nonnull Version version, @Nonnull final PlanResultKey planResultKey, @Nonnull final Map<String, String> settings)
            throws CredentialsRequiredException {
        Project project = version.getProject();

        final String projectKey = project.getKey();

        clearErrors(version);
        clearBuildData(projectKey, version.getId());
        saveConfigData(projectKey, version, settings);

        final Map<String, String> params = populateParams(version, settings, project);

        String stage = settings.get(PluginConstants.PS_CONFIG_STAGE);
        final RestResult<PlanResultKey> restResult = bambooRestService.continuePlan(applicationLink, planResultKey, stage, params);

        if (restResult.getErrors().isEmpty()) {
            String userName = settings.get(PS_CONFIG_USER_NAME);
            subscribeToReleaseStatus(version, userName, planResultKey);
            registerReleaseStarted(projectKey, version.getId(), planResultKey);
        }

        return new PlanExecutionResult(restResult.getResult(), restResult.getErrors());
    }

    public void releaseWithNoBuild(final Version version, final Map<String, String> releaseConfig) {
        final Project project = version.getProject();
        final String projectKey = project.getKey();
        final Long versionId = version.getId();

        clearErrors(version);
        clearConfigData(projectKey, versionId);
        clearBuildData(projectKey, versionId);
        saveConfigData(projectKey, version, releaseConfig);
        doJiraRelease(version, releaseConfig);
    }

    public boolean releaseIfRequired(@Nonnull final PlanResultStatus planResultStatus, @Nonnull final Version version) {
        try {
            return versionReleaseLocks.get(version).withLock(
                    (Callable<Boolean>) () -> releaseWithoutLock(version, planResultStatus));
        } catch (Exception e) {
            log.error("Unexpected Error Has Occured Performing Release", e);
        }

        return false;
    }

    public void onStart() {
        final Thread thread = new Thread(new ReleaseBuildSubscriber(), "Bamboo Release Management Startup");
        thread.setUncaughtExceptionHandler(
                (thread1, throwable) -> log.error("Could not start Bamboo Release Management Plugin", throwable));
        thread.start();
    }

    private class ReleaseBuildSubscriber implements Runnable {
        @Override
        public void run() {
            for (final Project project : projectManager.getProjectObjects()) {
                final Collection<Version> versions = project.getVersions();
                // As an optimisation, only perform the following queries if the project has any versions
                if (!versions.isEmpty()) {
                    final PluginSettings projectSettings = pluginSettingsFactory.createSettingsForKey(project.getKey());
                    final Collection<String> releaseBuildKeys = getReleaseBuildKeys(project);
                    for (final Version version : versions) {
                        subscribeToReleaseBuildUpdates(projectSettings, releaseBuildKeys, version);
                    }
                }
            }
        }

        private void subscribeToReleaseBuildUpdates(
                final PluginSettings projectSettings, final Collection<String> releaseBuildKeys, final Version version) {
            // To avoid making a DB query for every single version in every project, we check whether
            // the DB is known to contain build data for a given version BEFORE querying for that data.
            final String releaseBuildKey = PS_BUILD_DATA_KEY + version.getId();
            if (releaseBuildKeys.contains(releaseBuildKey)) {
                @SuppressWarnings("unchecked")
                final Map<String, String> releaseBuild = (Map<String, String>) projectSettings.get(releaseBuildKey);
                if (releaseBuild != null) {
                    final String buildResultKey = releaseBuild.get(PS_BUILD_RESULT);
                    final boolean isCompleted = releaseBuild.containsKey(PS_BUILD_COMPLETED_STATE);
                    final String username = releaseBuild.get(PS_CONFIG_USER_NAME);

                    if (isNotEmpty(buildResultKey) && isNotEmpty(username) && !isCompleted) {
                        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey(buildResultKey);
                        subscribeToReleaseStatus(version, username, planResultKey);
                    }
                }
            }
        }

        /**
         * Returns the PropertySet keys that contain release build information for versions of the given project.
         *
         * @param project the project for which to return the release build keys
         * @return any keys prefixed with "<code>{@value PluginConstants#PS_BUILD_DATA_KEY}</code>"
         */
        @SuppressWarnings("unchecked")
        private Collection<String> getReleaseBuildKeys(final Project project) {
            final PropertySet projectProperties = jiraPropertySetFactory.buildCachingDefaultPropertySet(project.getKey());
            return projectProperties.getKeys(PS_BUILD_DATA_KEY);
        }
    }

    public void onStop() {
        // Nothing to do
    }

    public void resetReleaseStateIfVersionWasUnreleased(@Nonnull Version version) {
        Project project = version.getProject();

        final String projectKey = project.getKey();
        final Long versionId = version.getId();

        if (!version.isReleased() && getBuildState(projectKey, versionId) == SUCCESS && isReleaseCompleted(projectKey, versionId)) {
            clearConfigData(projectKey, versionId);
            clearBuildData(projectKey, versionId);
        }
    }

    public void clearConfigData(@Nonnull String projectKey, long versionId) {
        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        settingsForKey.remove(PluginConstants.PS_CONFIG_DATA_KEY + versionId);
    }

    @Nullable
    public Map<String, String> getConfigData(@Nonnull String projectKey, long versionId) {
        return getConfigMap(projectKey, PluginConstants.PS_CONFIG_DATA_KEY + versionId);
    }

    @Nonnull
    public Map<String, String> getDefaultSettings(@Nonnull final String projectKey) {
        Map<String, String> settings = getConfigMap(projectKey, PluginConstants.PS_CONFIG_DEFAUTS_KEY);
        return (settings != null) ? settings : newHashMap();
    }

    @Nullable
    public Map<String, String> getBuildData(@Nonnull final String projectKey, long versionId) {
        return getConfigMap(projectKey, PS_BUILD_DATA_KEY + versionId);
    }

    public void registerReleaseStarted(@Nonnull String projectKey, long versionId, @Nonnull PlanResultKey planResultKey) {
        final Map<String, String> data = singletonMap(PS_BUILD_RESULT, planResultKey.getKey());

        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        settingsForKey.put(PS_BUILD_DATA_KEY + versionId, data);
    }

    /**
     * Records the end of a release build for this version.  Only called after entire release is complete.
     *
     * @param projectKey    of the project the version belongs to
     * @param versionId     of the version to remove associated configuration
     * @param planResultKey of the release build associated with this version
     * @param buildState    - the status of the completed bamboo build.
     */
    private void registerReleaseFinished(
            @Nonnull String projectKey, long versionId, @Nonnull PlanResultKey planResultKey, BuildState buildState) {
        final Map<String, String> data = new HashMap<>();
        data.put(PS_BUILD_RESULT, planResultKey.getKey());
        data.put(PS_BUILD_COMPLETED_STATE, buildState.name());
        data.put(PluginConstants.PS_RELEASE_COMPLETE, Boolean.TRUE.toString());

        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        settingsForKey.put(PS_BUILD_DATA_KEY + versionId, data);
    }

    public boolean hasPermissionToRelease(@Nullable final ApplicationUser user, @Nonnull final Project project) {
        return permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user) ||
                globalPermissionManager.hasPermission(ADMINISTER, user);
    }

    @SuppressWarnings("deprecation")
    public boolean hasPermissionToRelease(@Nullable final User user, @Nonnull final Project project) {
        return hasPermissionToRelease(ApplicationUsers.from(user), project);
    }

    @Nonnull
    public Map<String, String> getBambooVariablesFromMap(
            @Nonnull final Map<String, String> toFilter, @Nonnull final String prefixSubstitute) {
        return toFilter.entrySet().stream()
                .filter(entry -> entry.getKey().startsWith(VARIABLE_PARAM_PREFIX))
                .filter(entry -> isNotBlank(entry.getValue()))
                .collect(
                        toMap(
                                entry -> entry.getKey().replace(VARIABLE_PARAM_PREFIX, prefixSubstitute),
                                Entry::getValue
                        )
                );
    }

    @VisibleForTesting
    void doJiraRelease(final Version version, final Map<String, String> releaseConfig) {
        if (!version.isReleased()) {
            //move issues if required
            final String openIssuesAction = releaseConfig.get(PS_CONFIG_OPEN_ISSUES);
            if (PluginConstants.ISSUE_ACTION_MOVE.equals(openIssuesAction)) {
                String userName = releaseConfig.get(PS_CONFIG_USER_NAME);
                ApplicationUser user = userManager.getUserByName(userName);

                String newVersionId = releaseConfig.get(PluginConstants.PS_CONFIG_OPEN_ISSUES_VERSION);
                Version newVersion = versionManager.getVersion(Long.parseLong(newVersionId));

                try {
                    final Collection<Issue> issues = getUnresolvedIssues(version, user);
                    if (!issues.isEmpty()) {
                        for (final Issue issue : issues) {
                            // Need to look this up from the DB since we have DocumentIssues from the search.
                            final MutableIssue mutableIssue = issueManager.getIssueObject(issue.getId());
                            final Collection<Version> versions = mutableIssue.getFixVersions();
                            versions.remove(version);
                            versions.add(newVersion);
                            mutableIssue.setFixVersions(versions);
                            issueManager.updateIssue(user, mutableIssue,
                                    UpdateIssueRequest.builder().eventDispatchOption(ISSUE_UPDATED).sendMail(true).build());
                        }
                    }
                } catch (Exception e) {
                    final String message = authenticationContext.getI18nHelper().getText(JIRA_ERROR_NOT_RECOVERABLE_I18N_KEY, newVersion);
                    releaseErrorReportingService.recordError(version.getProject().getKey(), version.getId(), message);
                    log.error(message, e);
                    return;
                }
            }

            // We use the given date string without checking validity; already set by the webwork action
            final Date releaseDate = dateFieldFormat.parseDatePicker(releaseConfig.get(PS_CONFIG_RELEASE_DATE));
            final Version editedVersion = versionManager.editVersionReleaseDate(version, releaseDate);
            versionManager.releaseVersion(editedVersion, true);
        }
    }

    private Map<String, String> populateParams(
            final Version version, final Map<String, String> settings, final Project project) {
        final Map<String, String> params = newHashMap();

        final ApplicationUser user = authenticationContext.getLoggedInUser();

        params.put(CUSTOM_PLAN_TRIGGER_KEY, PLAN_TRIGGER);
        params.put(CUSTOM_TRIGGER_REASON_KEY, TRIGGER_REASON);
        params.put(BAMBOO_TRIGGER_REASON_JIRA_USERNAME, user.getName());
        params.put(BAMBOO_TRIGGER_REASON_JIRA_USER_DISPLAY_NAME, user.getDisplayName());
        params.put(BAMBOO_TRIGGER_REASON_JIRA_PROJECT_NAME, project.getName());
        params.put(BAMBOO_TRIGGER_REASON_JIRA_PROJECT_KEY, project.getKey());
        params.put(BAMBOO_TRIGGER_REASON_JIRA_VERSION_NAME, version.getName());
        params.put(BAMBOO_TRIGGER_REASON_JIRA_VERSION_ID, version.getId().toString());

        final String baseUrl = getApplicationProperties().getString(APKeys.JIRA_BASEURL);
        params.put(BAMBOO_TRIGGER_REASON_JIRA_BASE_URL, baseUrl);

        params.putAll(getBambooVariablesFromMap(settings, VARIABLE_REST_PREFIX));
        return params;
    }

    private boolean releaseWithoutLock(final Version version, final PlanResultStatus planResultStatus) {
        final Project project = version.getProject();

        boolean released = false;
        if (!version.isReleased()) {
            if (SUCCESS == planResultStatus.getBuildState()) {
                final Map<String, String> releaseConfig = getConfigData(project.getKey(), version.getId());

                log.info("Bamboo Release Plugin releasing version " + version.getName() + " of project " + project.getKey());

                if (releaseConfig != null) {
                    // release version
                    doJiraRelease(version, releaseConfig);
                    released = true;
                    // clear out config
                    clearConfigData(project.getKey(), version.getId());
                } else {
                    final String errorMessage =
                            authenticationContext.getI18nHelper().getText(BAMBOO_ERROR_RELEASE_RETRY_MANUALLY_I18N_KEY);
                    releaseErrorReportingService.recordError(project.getKey(), version.getId(), errorMessage);
                    log.error("Release build " + planResultStatus.getPlanResultKey() +
                            " completed but no record of triggering the release can be found.  Version was not released.");
                }
            }

            registerReleaseFinished(project.getKey(), version.getId(), planResultStatus.getPlanResultKey(),
                    planResultStatus.getBuildState() != null ? planResultStatus.getBuildState() : BuildState.UNKNOWN);
        }
        return released;
    }

    private Collection<Issue> getUnresolvedIssues(final Version version, final ApplicationUser user) {
        try {
            final JqlClauseBuilder builder =
                    JqlQueryBuilder.newBuilder().where().project(version.getProject().getId()).and().unresolved();
            builder.and().fixVersion(version.getId());

            final SearchResults<Issue> results = searchService.search(user, builder.buildQuery(), PagerFilter.getUnlimitedFilter());
            return results.getResults();
        } catch (final Exception e) {
            log.error("Exception whilst getting unresolved issues " + e.getMessage(), e);
        }

        return emptyList();
    }

    @Nullable
    private Map<String, String> getConfigMap(final String projectKey, final String configKey) {
        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        final Object o = settingsForKey.get(configKey);
        if (o != null && Map.class.isAssignableFrom(o.getClass())) {
            //noinspection unchecked
            return (Map<String, String>) o;
        } else {
            return null;
        }
    }

    private void subscribeToReleaseStatus(
            @Nonnull final Version version, @Nonnull final String username, @Nonnull final PlanResultKey planResultKey) {
        final ReleaseFinalisingAction action = new ReleaseFinalisingAction(
                this, authenticationContext.getI18nHelper(), releaseErrorReportingService, version.getId(), versionManager);
        planStatusUpdateService.subscribe(version, planResultKey, username, action);
    }

    private void saveConfigData(
            @Nonnull final String projectKey, @Nonnull final Version version, @Nonnull final Map<String, String> config) {
        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        settingsForKey.put(PluginConstants.PS_CONFIG_DATA_KEY + version.getId(), config);
        settingsForKey.put(PluginConstants.PS_CONFIG_DEFAUTS_KEY, config);
    }

    private void clearErrors(final Version version) {
        releaseErrorReportingService.clearErrors(version.getProject().getKey(), version.getId());
    }

    private boolean isReleaseCompleted(@Nonnull String projectKey, long versionId) {
        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        final Map<String, String> config = (Map<String, String>) settingsForKey.get(PS_BUILD_DATA_KEY + versionId);
        return config != null && config.containsKey(PluginConstants.PS_RELEASE_COMPLETE);
    }

    private BuildState getBuildState(@Nonnull String projectKey, long versionId) {
        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        final Map<String, String> config = (Map<String, String>) settingsForKey.get(PS_BUILD_DATA_KEY + versionId);
        if (config != null) {
            String state = config.get(PS_BUILD_COMPLETED_STATE);
            if (isNotEmpty(state)) {
                try {
                    return BuildState.valueOf(state);
                } catch (IllegalArgumentException e) {
                    log.debug("Could not determine BuildState from Plugin Settings. Falling back to 'UNKNOWN'", e);
                }
            }
        }
        return BuildState.UNKNOWN;
    }

    private void clearBuildData(@Nonnull String projectKey, long versionId) {
        final PluginSettings settingsForKey = pluginSettingsFactory.createSettingsForKey(projectKey);
        settingsForKey.remove(PS_BUILD_DATA_KEY + versionId);
    }
}
