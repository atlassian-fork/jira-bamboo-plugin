package com.atlassian.jira.plugin.ext.bamboo.rest;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.plugin.ext.bamboo.model.ErrorMessage;
import com.atlassian.jira.plugin.ext.bamboo.model.OAuthErrorMessage;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooRestService;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_UNREACHABLE_TITLE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.LOGIN_AND_APPROVE_BEFORE_STATUS_VISIBLE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_COMPLETED_STATE;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Path("/status")
public class BambooStatusResource {
    private static final Logger log = Logger.getLogger(BambooStatusResource.class);

    private static final String BAMBOO = "bamboo";
    private static final String JIRA = "jira";

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooReleaseService bambooReleaseService;
    private final BambooRestService bambooRestService;
    private final I18nResolver i18nResolver;
    private final VersionManager versionManager;

    public BambooStatusResource(
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final VersionManager versionManager,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooReleaseService bambooReleaseService,
            final BambooRestService bambooRestService) {
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooReleaseService = checkNotNull(bambooReleaseService);
        this.bambooRestService = checkNotNull(bambooRestService);
        this.i18nResolver = checkNotNull(i18nResolver);
        this.versionManager = checkNotNull(versionManager);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{jiraVersionId}/{planResultKey}")
    public Response getStatus(
            @PathParam("planResultKey") final String resultKey,
            @PathParam("jiraVersionId") final long jiraVersionId) {
        final Version version = versionManager.getVersion(jiraVersionId);
        if (version == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        final ApplicationLink bambooApplicationLink =
                bambooApplicationLinkManager.getApplicationLink(version.getProject().getKey());

        if (bambooApplicationLink == null) {
            return Response.serverError().build();
        }

        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey(resultKey);

        try {
            final RestResult<JSONObject> restResult = bambooRestService.getPlanResultJson(
                    bambooApplicationLink.createAuthenticatedRequestFactory(), planResultKey);
            JSONObject bambooJsonObject = restResult.getResult();

            if (bambooJsonObject == null) {
                final ErrorMessage errorMessage = new ErrorMessage(
                        i18nResolver.getText(BAMBOO_UNREACHABLE_TITLE_I18N_KEY),
                        restResult.getErrorMessage(i18nResolver.getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, bambooApplicationLink.getName())));
                return errorMessage.createJSONEntity(Response.status(Response.Status.NOT_FOUND)).build();
            }

            PlanResultStatus planResultStatus = getPlanResultStatusFromJSON(planResultKey, bambooJsonObject);

            JSONObject jiraJsonObject = new JSONObject();
            if (!version.isReleased() && planResultStatus.getBuildState() != BuildState.UNKNOWN) {
                // check if we are waiting for it to be released
                final Map<String, String> buildParams =
                        bambooReleaseService.getBuildData(version.getProject().getKey(), version.getId());
                if (buildParams != null) {
                    String state = buildParams.get(PS_BUILD_COMPLETED_STATE);
                    // if state is not null then we have already release this plan, we dont have to do it again...

                    if (state == null) {
                        bambooReleaseService.releaseIfRequired(planResultStatus, version);
                        jiraJsonObject.put("forceRefresh", true);
                    }
                }

                //Lets re-look this up to be sure we are consistent
                jiraJsonObject.put("released", versionManager.getVersion(jiraVersionId).isReleased());

            } else {
                jiraJsonObject.put("released", version.isReleased());
            }

            final JSONObject result = new JSONObject();
            result.put(BAMBOO, bambooJsonObject);
            result.put(JIRA, jiraJsonObject);

            return Response.ok(result.toString(), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (CredentialsRequiredException e) {
            log.debug("Credentials required", e);
            final String description = i18nResolver.getText(LOGIN_AND_APPROVE_BEFORE_STATUS_VISIBLE_I18N_KEY);
            return new OAuthErrorMessage(description, e.getAuthorisationURI()).createJSONEntity(Response.status(UNAUTHORIZED)).build();
        } catch (JSONException e) {
            log.error("Failed to parse status response from Bamboo", e);
            return Response.serverError().build();
        } catch (RuntimeException e) {
            log.error("Unexpected server error", e);
            return Response.serverError().build();
        }
    }

    protected PlanResultStatus getPlanResultStatusFromJSON(PlanResultKey planResultKey, JSONObject bambooJsonObject)
            throws JSONException {
        return PlanResultStatus.fromJsonObject(planResultKey, bambooJsonObject);
    }
}
