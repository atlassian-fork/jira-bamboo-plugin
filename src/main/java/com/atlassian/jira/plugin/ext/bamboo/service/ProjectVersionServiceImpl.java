package com.atlassian.jira.plugin.ext.bamboo.service;

import io.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

@Component
public class ProjectVersionServiceImpl implements ProjectVersionService {
    @VisibleForTesting
    static final PagerFilter UNLIMITED_FILTER = PagerFilter.getUnlimitedFilter();

    @VisibleForTesting
    static final String ADMIN_ERROR_KEY = "admin.errors.project.exception";

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectVersionService.class);

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SearchService searchService;

    @Autowired
    public ProjectVersionServiceImpl(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final SearchService searchService) {
        this.jiraAuthenticationContext = checkNotNull(jiraAuthenticationContext);
        this.searchService = checkNotNull(searchService);
    }

    @Override
    @Nonnull
    public Either<String, Integer> getUnresolvedIssueCount(final long projectId, final long versionId) {
        try {
            final Query query = JqlQueryBuilder.newBuilder()
                    .where().project(projectId)
                    .and().unresolved()
                    .and().fixVersion(versionId)
                    .buildQuery();
            final SearchResults results =
                    searchService.search(jiraAuthenticationContext.getLoggedInUser(), query, UNLIMITED_FILTER);
            return Either.right(results.getTotal());
        } catch (final Exception e) {
            LOGGER.error("Exception whilst getting unresolved issues " + e.getMessage(), e);
            return Either.left(jiraAuthenticationContext.getI18nHelper().getText(ADMIN_ERROR_KEY, e));
        }
    }
}
