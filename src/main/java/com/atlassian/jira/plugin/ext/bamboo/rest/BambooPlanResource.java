package com.atlassian.jira.plugin.ext.bamboo.rest;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.ext.bamboo.PluginConstants;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.ErrorMessage;
import com.atlassian.jira.plugin.ext.bamboo.model.OAuthErrorMessage;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooRestService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_UNREACHABLE_TITLE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.LOGIN_AND_APPROVE_BEFORE_CONTINUING_I18N_KEY;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Path("/plan")
public class BambooPlanResource {
    private static final Logger log = Logger.getLogger(BambooPlanResource.class);

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooRestService bambooRestService;
    private final I18nResolver i18nResolver;

    public BambooPlanResource(
            @ComponentImport final I18nResolver i18nResolver,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooRestService bambooRestService) {
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooRestService = checkNotNull(bambooRestService);
        this.i18nResolver = checkNotNull(i18nResolver);
    }

    @GET
    @Path("{jiraProjectKey}/{planKey}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlan(@PathParam("jiraProjectKey") final String projectKey, @PathParam("planKey") final String key) {
        final ApplicationLink bambooApplicationLink = bambooApplicationLinkManager.getApplicationLink(projectKey);

        if (bambooApplicationLink == null) {
            return Response.serverError().build();
        }

        final PlanKey planKey = PlanKeys.getPlanKey(key);

        try {
            final RestResult<JSONObject> result =
                    bambooRestService.getPlanJson(bambooApplicationLink.createAuthenticatedRequestFactory(), planKey);
            final JSONObject jsonObject = result.getResult();
            if (jsonObject == null) {
                final ErrorMessage errorMessage = new ErrorMessage(
                        i18nResolver.getText(BAMBOO_UNREACHABLE_TITLE_I18N_KEY),
                        result.getErrorMessage(i18nResolver.getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, bambooApplicationLink.getName())));
                return errorMessage.createJSONEntity(Response.status(NOT_FOUND)).build();
            }
            return Response.ok(jsonObject.toString(), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (final CredentialsRequiredException e) {
            log.debug(PluginConstants.CREDENTIALS_REQUIRED, e);
            final String description = i18nResolver.getText(LOGIN_AND_APPROVE_BEFORE_CONTINUING_I18N_KEY);
            return new OAuthErrorMessage(description, e.getAuthorisationURI()).createJSONEntity(Response.status(UNAUTHORIZED)).build();
        } catch (RuntimeException e) {
            log.error("Unexpected server error", e);
            return Response.serverError().build();
        }
    }
}
